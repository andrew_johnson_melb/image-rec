
import sys
# extra path added as pycharm requires 
# the full path however, this is different 
# when running via the cmd line.  
sys.path.add('..')

import torch
import argparse
import yaml
from ml_engine.src.data import create_test_and_train_sets
from ml_engine.src.model import train_model


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='train CNN model')
    parser.add_argument('--path', type=str,
            help='base path to the training data',
            default='../data')
    parser.add_argument('--save_dir', type=str,
            help='The dir where the model will be saved',
            default='../models/checkpoint.pt')
    parser.add_argument('--gpu', type=bool, default=True,
            help='Whether or not to use the GPU for training')
    parser.add_argument('--model_config', type=str,
            help='Path to config file storing model params',
            default='../ml_engine/config/model_config.yml')
    args = parser.parse_args()
    return args


def main(args, model_config, device):

    # Load in the image data, create batch generators
    data_loaders, image_data, _ = \
            create_test_and_train_sets(base_dir=args.path,
                                       batch_size=model_config["batch_size"])

    # Create and train the 'image_net' CNN model.
    model, optim, loss_results = train_model(
                                    model_config,
                                    data_loaders=data_loaders,
                                    image_data=image_data,
                                    device=device
                                    )
    
    # Save the state of the model post-training
    model_info = {
        'epoch': model_config["epochs"],
         'arch' : model_config['arch'],
        'loss_results' : loss_results,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optim.state_dict(),
        'class_to_idx' : image_data['train'].class_to_idx
    }

    torch.save(model_info, args.save_dir)


if __name__ == '__main__':
    # Load the input params
    args = parse_arguments()

    # Load the model params
    with open(args.model_config, 'r') as stream:
        model_config = yaml.load(stream)
    
    # Set the correct device
    if args.gpu:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print('Using device {}'.format(device))
    else:
        device = torch.device("cpu")
        print('Using device {}'.format(device))

    main(args, model_config, device)

