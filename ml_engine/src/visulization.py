
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image


def imshow(inp, title=None):
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.figure(figsize=(10,10))
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated


def display_image_and_probs(image_path, top_N_cats, top_N_prob, save_dir=None):
    """
    Create a plot of the input image and
    the top 5 predicted flower species.
    """
    input_image_raw = Image.open(image_path)
    f, ax = plt.subplots(2, sharex=False, figsize=(10, 10))
    ax[1].bar(x=top_N_cats, height=top_N_prob.flatten())
    ax[0].imshow(input_image_raw)
    if save_dir is not None:
        plt.savefig(save_dir + 'model_predictions.pdf')

