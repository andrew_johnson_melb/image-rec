
import torch
from torch import optim

def load_checkpoint(path, model, device):
    """
    Restore a saved model and it's current training state
    """
    # Get the check point data.
    checkpoint = torch.load(path)
    # Create a new model and optimizer.
    model.load_state_dict(checkpoint['model_state_dict'])
    # create optimizer.
    optimizer = optim.Adam(model.classifier.parameters(), lr=0.001)
    # add state to optimizer.
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    # Pin to device
    model = model.to(device)

    return model, optimizer, checkpoint