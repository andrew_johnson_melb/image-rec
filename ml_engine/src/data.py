

from torchvision import datasets
from torchvision import transforms
from torch.utils.data import DataLoader

IMAGE_MEANS = [0.485, 0.456, 0.406]
IMAGE_STD = [0.229, 0.224, 0.225]
DATA_SETS = ['train', 'val', 'test']


def create_test_and_train_sets(base_dir, batch_size=64):
    """
    Loads the train/val/test data sets for training the image
    classifier

    # Arguments
        data_dir: Full path to the image data

    # Returns
        dicts (data_loaders, image_data) containing
        DataLoader and datasets.ImageFolder for keys
        'train', 'val', 'test'

    """

    data_dirs = {
        'train': base_dir + '/train',
        'val': base_dir + '/valid',
        'test': base_dir + '/test'
    }

    # Create data transformations for input images.
    # train set transformations contains standard
    # data augmentation tricks
    data_transforms = {}
    data_transforms['train'] = transforms.Compose([
        transforms.RandomRotation(30),
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_MEANS, IMAGE_STD)])

    # Transforms for testing and validation data.
    data_transforms['val'] = transforms.Compose([transforms.Resize(226),
                                                 transforms.CenterCrop(224),
                                                 transforms.ToTensor(),
                                                 transforms.Normalize(IMAGE_MEANS, IMAGE_STD)])

    # The transformations for the test and validation sets
    # are the same.
    data_transforms['test'] = data_transforms['val']

    # Read in the image data.
    image_data = {}
    for data_label in DATA_SETS:
        image_data[data_label] = datasets.ImageFolder(
            root=data_dirs[data_label], transform=data_transforms[data_label])

    # Compute the size of each dataset
    dataset_sizes = {x: len(image_data[x]) for x in DATA_SETS}

    # Create batch loaders for the image data
    data_loaders = {}
    for data_label in DATA_SETS:
        data_loaders[data_label] = DataLoader(image_data[data_label],
                                              batch_size=batch_size,
                                              shuffle=True)

    return data_loaders, image_data, dataset_sizes

