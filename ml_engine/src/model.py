
import torch
from torchvision import models
from torch import nn
from collections import OrderedDict
from torch import optim
from ml_engine.src.image_utils import process_image


LAYER_MAP = {
        'vgg16' : {'layer_name' : 'classifier', 'input_dim' : 25088},
        'vgg19' : {'layer_name' : 'classifier', 'input_dim' : 25088}
        }


def create_basic_classifier(num_features, input_features, 
                     hidden_layer_size, p_dropout):
    """ 
    Create a single layer classifier with dropout 
    and Relu activation functions. 
    
    This network outputs values as LogSoftmax 
    (to avoid numerical issues).
    
    # Parameters
        num_features: int, the number of features to predicted.
        input_features: int, the number of input features to
            the network.
        hidden_layer_size: size of the hidden layer. 
        p_dropout: probaility of dropout

    # Returns
        classifier, pytorch model with specified architecture

    """
    print ('Creating classifier with structure..')
    print ('Hidden layer size = {}'.format(hidden_layer_size))
    print ('probability of dropout = {}'.format(p_dropout))
    
    classifier = nn.Sequential(OrderedDict([
        ('fc1', nn.Linear(in_features=input_features, 
                          out_features=hidden_layer_size, bias=True)),
        ('dropout_1', nn.Dropout(p_dropout)),
        ('relu', nn.ReLU()),
        ('fc2', nn.Linear(in_features=hidden_layer_size, 
                          out_features=num_features)),
        ('output', nn.LogSoftmax(dim=1))
        ]))

    return classifier


def image_net(num_features, 
              arch='vgg16',
              hidden_layer_size=4096,
              p_dropout=0.5,
              use_pretrained=True):
    """
    Creates a CNN classification model using a pretrained 
    network where the final classification layer is changed 

    # Parameters
        arch: str, name of the pretrained network to use. 
            The options are defined in the layer map.
        hidden_layer_size: int, the number of hidden layers to use 
            in the single layer classification layer, i.e., the new network                  build ontop of the pre-trained network (default, 4096).
        p_dropout: float, The dropout probability of the hidden layer 
            of the new classification layer (default)
        use_pretrained: bool, Whether or not to use the pretrained 
            weights are bias.
    # Returns
        model: pytorch model
    # Raises
        NotImplementedError: if an unknown architecture is input.

    """
    # Check to see if the architecture has
    # been mapped to a classifier layer name.
    if arch not in LAYER_MAP.keys():
        raise NotImplementedError('{} architecture not implemented!'.format(arch)          )
    # Load the pretrained network
    model = getattr(models, arch)(pretrained=use_pretrained)
    # Fix all the parameters in this network
    # by not requiring any Grad.
    for param in model.parameters():
        param.requires_grad = False  
    # Find the numnber of features input intp the classifier.
    num_input_features = LAYER_MAP[arch]['input_dim']
    # Create the output classifier.
    classifier = create_basic_classifier(num_features, 
                            input_features=num_input_features,
                            p_dropout = p_dropout,
                            hidden_layer_size=hidden_layer_size)
    # Update CNN with new classification layer
    print ('Adding classification layer..')
    model.classifier = classifier
    return model


def train_model(args, data_loaders, image_data, device):
    """
    Train the ImageNet model.

    # Parametes
        args: dict, defines parameters of model are training steps. 
        data_loaders: torch.data_loaders instance, used 
            to load batch of the data.
        image_data: torch.dataset instance, contains full dataset.
        device: str, "gpu" or "cpu" device to run the training on

    # Returns
        model: trained pytorch model
        optimizer: state of optimizer post training NN
        loss_results: dict, contains loss information from 
            training and validation sets.

    """
    # Retrieve training params.
    learning_rate = args["learning_rate"]
    num_epochs = args['num_epochs']

    # Compute the sizes of each of the datasets
    dataset_sizes = {x: len(image_data[x]) for x in ['train', 'val']}

    # Find the number of classes to classify
    num_features = len(image_data['train'].classes)

    # Create the model
    model = image_net(num_features, 
                      arch=args['arch'],
                      hidden_layer_size=args['hidden_layer_size'],
                      p_dropout=args['p_dropout'])

    # pin the model to the GPU is it's available.
    model = model.to(device)

    # Use the negative log-loss loss
    # since the output of the network is log-softmax
    criterion = nn.NLLLoss()

    # Only update the classification parameters.
    optimizer = optim.Adam(model.classifier.parameters(),
                           lr=learning_rate)

    # Store all the loss results
    loss_results = []
    for epoch in range(num_epochs):

        print('Epoch num {} / {}'.format(epoch, num_epochs - 1))

        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()
            else:
                # Layers like batch norm and dropout
                # behave differently when the model is
                # in eval mode. e.g., dropout will
                # be turned off.
                model.eval()

            # Within each epoch/training phase we
            # will keep track of the loss
            running_loss = 0.0
            running_corrects = 0

            # Read in the data
            # We have the input images and there
            # Associated class labels
            for inputs, classes in data_loaders[phase]:

                # Transfer the batch data to the GPU
                inputs = inputs.to(device)
                classes = classes.to(device)

                # Zero the parameter gradients
                # Backwards accumulates gradients, and we
                # don't want this to occur between batchs.
                optimizer.zero_grad()

                # If we are not training all grads are turned off.
                # forward track history if only in train
                # model.eval() does not turn off gradient calculations
                # so we will do it here.

                with torch.set_grad_enabled(phase == 'train'):

                    output = model.forward(inputs)
                    loss = criterion(output, classes)

                    # Get the predicted class, by finding
                    # the highest prob class.
                    _, preds = torch.max(torch.exp(output), 1)

                    if phase == 'train':
                        # update all weights if training..
                        loss.backward()
                        optimizer.step()

                # Compute running loss and accuracy
                # item converts 1 elements tensors to
                # python scalars.

                # This is the loss on 'batch_size' items.
                # need to muliple the loss by 'batch_size'
                # to use 'dataset_sizes' to normalize

                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == classes.data)

            # Compute epoch level loss and accuracy.
            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = 100.0 * (running_corrects.float() / dataset_sizes[phase])

            print('Phase = {} loss = {:.4f}, accuracy = {:.4f} %'.format(
                phase, epoch_loss, epoch_acc))

            # Keep record of all training results.
            epoch_results = {
                'accuracy': epoch_acc,
                'loss': epoch_loss,
                'epoch' : epoch,
                'set' : phase }

            loss_results.append(epoch_results)

    return model, optimizer, loss_results



def evaluate_model(model, data_loaders, dataset_sizes, device, phase='test'):
    """
    Compute the accuracy and log-loss using the input model
    and the input dataset.

    Note, the data augmentation may cause a large
     difference between the training and test set
     this will introduce noise between the two.

    """
    criterion = nn.NLLLoss()

    model.eval()
    running_loss = 0.0
    running_corrects = 0

    for inputs, classes in data_loaders[phase]:
        inputs = inputs.to(device)
        classes = classes.to(device)
        with torch.set_grad_enabled(False):
            output = model.forward(inputs)
            loss = criterion(output, classes)
            _, preds = torch.max(torch.exp(output), 1)

        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == classes.data)

    # Compute epoch level loss and accuracy.
    loss = running_loss / dataset_sizes[phase]
    accuracy = 100.0 * (running_corrects.float() / dataset_sizes[phase])

    return loss, accuracy


def predict(image_path, model, device, topk=5):
    ''' 
    Predict the class (or classes) of an image 
    using a trained deep learning model.

    # Parameters
        image_path: str, full path to the image 
            file to predict
        model: trained 'image_net' model
        device: str, decide to use
        topk: int, top N to return (default 5)
    # Returns
        top_probs: list, probabilities of the top 'topk' flowers. 
        top_class_labels: list, name of the top 'topk' flowers. 
    
    '''
    # load image and perform pre-processing.
    input_image = Image.open(image_path)
    input_image = process_image(input_image)
    input_image = input_image.reshape((1, 3, 224, 224))
    input_image = input_image.float()
    # Make sure the model is in eval mode
    # or the results will not the fixed.
    model.eval()
    input_image_batch = input_image.to(device)
    model = model.to(device)
    # Turn off grad tracking
    with torch.set_grad_enabled(False):
        output = model.forward(input_image_batch)
        probs = torch.exp(output)
        top_probs, top_idx = probs.topk(topk)
    # Compute the top N probs and class names.
    # Send to CPU and Convert to numpy arrays
    top_probs = top_probs.to("cpu").numpy()
    top_idx = top_idx.to("cpu").numpy()
    # Create a map from idx to class label
    map_idx_to_class = {idx : class_label for
                    class_label, idx in model.class_to_idx.items()}
    # Convert from the index to the class label
    top_class_labels = [map_idx_to_class[idx] for idx in top_idx.flatten()]
    return top_probs, top_class_labels


