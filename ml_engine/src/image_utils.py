
import torch
import numpy as np

DEFAULT_CHANNEL_MEANS = [0.485, 0.456, 0.406]
DEFAULT_CHANNEL_STD = [0.229, 0.224, 0.225]


def centre_crop_shape(im, new_width, new_height):
    width, height = im.size   # Get dimensions
    left = (width - new_width)/2
    top = (height - new_height)/2
    right = (width + new_width)/2
    bottom = (height + new_height)/2
    im = im.crop((left,top, right, bottom))
    return im


def process_image(image, channel_means=None, channel_std=None):
    """
    Scales, crops, and normalizes a PIL image for a PyTorch model,
    returns an Numpy array.
    """
    if channel_means is None:
        channel_means = DEFAULT_CHANNEL_MEANS
    if channel_std is None:
        channel_std = DEFAULT_CHANNEL_STD
    # Square
    image = image.resize(size=(256, 256))
    # Crop out central 224 by 224 pixel values.
    image = centre_crop_shape(image, new_width=224, new_height=224)
    # convert to numpy array.
    np_image = np.array(image)
    # Normalizae range to 0-1 from 0-255
    np_image = np_image / 255.
    # Adjust channel means and standard ddeviation.
    flat_images = np_image.reshape(224 * 224, 3)
    flat_images = (flat_images - channel_means) / channel_std
    np_image = flat_images.reshape(224, 224, 3)
    # Update axis order to conform with pytorch.
    np_image_transposed = np_image.transpose(2, 0, 1)
    # Convert to a pytorch tensor
    return torch.from_numpy(np_image_transposed)
