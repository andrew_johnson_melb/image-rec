
import sys
# extra path added as pycharm requires 
# the full path however, this is different 
# when running via the cmd line.  
sys.path.add('..')

from ml_engine.src.utils import load_checkpoint
from ml_engine.src.model import image_net
from ml_engine.src.model import predict
from ml_engine.src.visulization import display_image_and_probs

import argparse
import torch
import json

NUM_FEATURES = 102

def argument_parser():

    parser = argparse.ArgumentParser(description='predict flower cat')
    parser.add_argument('--image_dir', type=str,
                        help='path to the image to predict',
                        default='../data/example/image_03295.jpg')
    parser.add_argument('--checkpoint', type=str,
                        help='path to the saved model',
                        default='../models/checkpoint.pt')
    parser.add_argument('--top_N', type=bool, default=5,
                        help='Number of top class estimates to return')
    parser.add_argument('--category_map', type=bool,
                        default='../data/cat_to_name.json',
                        help='Path to map between class values and class names')
    parser.add_argument('--gpu', type=bool, default=True,
            help='Whether or not to use the GPU.')
    args = parser.parse_args()
    return args


def main(args, device):

    # Create a new base model.
    model = image_net(num_features=NUM_FEATURES)
    model,_, checkpoint = load_checkpoint(args.checkpoint, model, device)

    # Get the categories names
    with open(args.category_map, 'r') as f:
        cat_to_name = json.load(f)

    # Attach the index mapping to the model
    model.class_to_idx = checkpoint['class_to_idx']

    # Get the top N predictions for the input image.
    topN_prob, topN_classes = predict(args.image_dir, model, args.top_N)

    # Convert the numeric ID to a flower name.
    top_5_cats = [cat_to_name[class_label] for class_label in topN_classes]

    # Print out the top 5 categories and there probabilities
    # Should really JSONify this and return it.

    topN_suggestions = json.dumps(dict(zip(top_5_cats,topN_prob)))
    print ('Predictions...', topN_suggestions)

    # Create a plot of the image and it's predicted types.
    display_image_and_probs(args.image_dir, top_5_cats, topN_prob)


if __name__ == '__main__':
    # Load the input params
    args = argument_parser()
        # Set the correct device
    if args.gpu:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print('Using device {}'.format(device))
    else:
        device = torch.device("cpu")
        print('Using device {}'.format(device))

    main(args, device)
